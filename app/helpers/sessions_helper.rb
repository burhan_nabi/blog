module SessionsHelper

  # Method to login to temporary sessions
  def log_in(author)
    _author = Author.find_by(email: author.email)
    if( _author)
      session[:author_id] = author.id
    else
      user = author
      session[:user_id] = user.id
    end
  end

  # Returns true if the given user is the current user.
  def current_user?(user)
    user == current_user
  end

  # Returns the current author if any
  def current_author
    @current_author ||= Author.find_by( id: session[:author_id] )
  end

  # Returns the current user if any
  def current_user
    @current_user ||= User.find_by( id: session[:user_id] )
  end

  # Returns true if an author is logged in, false otherwise
  def logged_in?
    !current_author.nil? || !current_user.nil?
  end

  def store_location
    session[:forwarding_url] = request.url if request.get?
  end


  #Logs the user out, by setting current use to nil
  def log_out
    session.delete(:author_id)
    session.delete(:user_id)
    @current_author = nil
    @current_user = nil
    flash[:success] = 'Succesfully Logged Out'
  end
end
