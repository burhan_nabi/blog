module ArticlesHelper
  def current_article
    @current_article = @current_article || @article
  end
end
