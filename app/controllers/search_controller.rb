class SearchController < ApplicationController

  def search_articles
    if params[:q].nil?
      @articles = []
    else
      @articles = Article.search params[:q]
    end
  end
end
