class SessionsController < ApplicationController

  include SessionsHelper

  def new
  end

  def create
    store_location
    if(params[:session][:email].nil?)
      redirect_to 'new'
    end
    author = Author.find_by(email: params[:session][:email].downcase )

    if author and author.authenticate(params[:session][:password])
      # using log_in method in the SessionsHelper module
      log_in author
      redirect_back_or author
    elsif
      user = User.find_by(email: params[:session][:email].downcase )
      if(params[:session][:password])
        if user and user.authenticate(params[:session][:password])
          log_in user
          redirect_back_or user
        end
      else
        if user and user.password_digest == params[:session][:id]
          log_in user
          redirect_back_or user
        end
      end
    else
      flash.now[:danger] = 'Invalid email/password combination. Make sure
                            you have signed up before you log in.'
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_path
  end

  # Redirects to stored location (or to the default).
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # Stores the URL trying to be accessed.
  # def store_location
  #   session[:forwarding_url] = request.url if request.get?
  # end

  def upvote
    comment = Comment.find(params[:id])
    like = current_user.likes.find_by_comment_id(params[:id])
    if(like.nil?)
      current_user.likes.create(comment_id: params[:id],vote_type: 1)
    else
      comment.num_likes+=1
      comment.save!
      like.vote_type = 1
      like.save!
      comments = Article.find(comment.article_id).comments
      # render :partial=>'shared/comments', :locals=> { :@article=>Article.find(comment.article_id) }
      redirect_to Article.find(comment.article_id)
      return
    end
    if(comment and comment.num_likes.nil?)
      comment.num_likes = 0
    end
    comment.num_likes+=1
    comment.save!
    redirect_to Article.find(comment.article_id)
  end

  def downvote
    comment = Comment.find(params[:id])
    like = current_user.likes.find_by_comment_id(params[:id])
    if( like.nil?)
      current_user.likes.create(comment_id: params[:id],vote_type: -1)
    else
      comment.num_likes-=1
      comment.save!
      like.vote_type = -1
      like.save!
      redirect_to Article.find(comment.article_id)
      return
    end
    if(comment and comment.num_likes.nil?)
      comment.num_likes=0
    end
    comment.num_likes-=1
    comment.save!
    redirect_to Article.find(comment.article_id)
  end


  def toggleFav
    article = Article.find(params[:id])
    fav = current_user.favourites.find_by_article_id(params[:id])
    if fav
      if fav.loves
        fav.loves = false
      else
        fav.loves = true
      end
    else
      fav = current_user.favourites.create( article_id: params[:id], loves:true)
      fav.loves = true
    end
    fav.save!
    redirect_to article
  end

end
