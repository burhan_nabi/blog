class ArticlesController < ApplicationController

  require 'will_paginate/array'

  def new
    @article = Article.new
  end

  def show
    @article = Article.find(params[:id])
  end

  def create
    # @article = Article.new(params[:user])  not the final implementation security issues
    @article = Article.new( article_params ) # Safer
    if @article.save
      flash[:success] = 'New Article successfully posted'
      redirect_to @article
    else
      render 'new'
    end
  end


  def index
    @articles = Article.order('created_at DESC').all
  end

  private
    def article_params
      params.require(:article).permit(:name, :content)
    end
end
