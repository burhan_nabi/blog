class MainController < ApplicationController
  def home
    @admin = true
    @articles = Article.order('created_at DESC').limit(4)
  end

  def contact
  end
end
