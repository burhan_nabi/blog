class UsersController < ApplicationController
  before_filter :url_hack, only: [:show]

  def new
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
  end

  def create
    up = user_params
    if(up[:id].nil?)
      @user = User.create(name: up[:name], email: up[:email],
      password: up[:password], password_confirmation: up[:password_confirmation])
    else
      @user = User.create(name: up[:name], email: up[:email],
        password: 'whatever', password_confirmation: 'whatever',
                          password_digest: up[:id])
    end
    if @user.save
      session[:user_id] = @user.id
      redirect_to @user
    else
      render 'users/new'
    end
  end



  private
    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation, :id)
    end

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end

    def url_hack

      unless params[:id].to_i == session[:user_id] || params[:id].to_i ==
          session[:author_id]
        # This line redirects the user to the previous action
        unless session[:user_id] || session[:author_id]
          flash[:warning] = 'Please log in!'
        else
          flash[:warning] = 'Dear user! Please mind your own!'
        end
        redirect_to root_path
      end
    end

end
