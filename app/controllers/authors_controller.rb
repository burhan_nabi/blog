class AuthorsController < ApplicationController

  before_filter :url_hack, only: [:show]

  def new
    @author = Author.new
    @author.authenticated = false
  end

  def show
    @author = Author.find(params[:id])
  end

  def create
    @author = Author.new( author_params)
    if @author.save
      log_in @author
      redirect_to @author
    else
      render 'authors/new'
    end
  end

  private
    def author_params
      params.require(:author).permit(:name, :email, :password, :about,
              :email ,:password_confirmation)
    end

    def url_hack
      unless params[:id].to_i == session[:user_id] || params[:id].to_i ==
          session[:author_id]
        # This line redirects the user to the previous action
        unless session[:user_id] || session[:author_id]
          flash[:warning] = 'Please log in!'
        else
          flash[:warning] = 'Dear user! Please mind your own!'
        end
        redirect_to root_path
      end
    end

end
