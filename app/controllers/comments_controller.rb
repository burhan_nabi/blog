class CommentsController < ApplicationController

  def create
    @article = Article.find(params[:article_id])
    @comment = @article.comments.build(comment_params )
    if(@comment.valid?)
      @comment.num_likes = 0
      @comment.save
      flash[:success] = 'Comment successfully created'
      redirect_to article_path(@article)
    else
      flash[:danger] = 'Please enter a valid comment'
      redirect_to article_path(@article)
    end
  end

  private
    def comment_params
      params.require(:comment).permit(:name, :comment_text)
    end
end
