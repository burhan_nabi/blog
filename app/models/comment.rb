class Comment < ActiveRecord::Base
  belongs_to :article

  has_many :likes
  has_many :users, through: :likes

  validates :name, presence: true, length: { in:4..30}
  validates :comment_text, presence: true, length: { in:4..1200 }
end
