class Author < ActiveRecord::Base
  has_many :articles

  before_save { self.email = email.downcase}

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  validates :name, presence: true, length: { in:4..30 }
  validates :email, presence: true, length: {in:4...200},
            format: { with:VALID_EMAIL_REGEX }, uniqueness: {case_sensitive: false}
  validates :password, presence: true, length: {in:6..100}
  validates :password_confirmation, presence: true, length: {in:6..100}
  has_secure_password

  # Returns hash digest of a given string
  def Author.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
        BCrypt::Engine.cost
    BCrypt::Password.create( string, cost: cost)
  end
end
