class User < ActiveRecord::Base
  has_many :likes
  has_many :comments, through: :likes

  has_many :favourites
  has_many :articles, through: :favourites

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  before_save { self.email = email.downcase}

  validates :name, presence: true, length: {minimum:4, maximum: 30}

  validates :email, presence: true, length: {minimum:6, maximum: 200},
                    format: {with: VALID_EMAIL_REGEX},
                    uniqueness: { case_sensitive:false }

  validates :password, presence: true, length: {minimum: 6, maximum: 50}

  has_secure_password
end
