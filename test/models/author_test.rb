require 'test_helper'

class AuthorTest < ActiveSupport::TestCase
  def setup
    @author = Author.new(name:'Burhan Nabi', email:'nabi@burhan.com', about:'Air of mystery about the user..',
                          password:'foobar', password_confirmation:'foobar')
  end

  test 'author should be valid' do
    assert @author.valid?
  end

  test 'author name should be present' do
    @author.name = ''
    assert_not @author.valid?
  end

  test 'author email should be present' do
    @author.email = ''
    assert_not @author.valid?
  end

  test 'author name should not be too short' do
    @author.name = 'a'*3
    assert_not @author.valid?
  end

  test 'author name should not be too long' do
    @author.name = 'a'*31
    assert_not @author.valid?
  end

  test 'author email should not be too short' do
    @author.email = 'a'*3
    assert_not @author.valid?
  end

  test 'author email should not be too long' do
    @author.email = 'a'*201
    assert_not @author.valid?
  end

  test 'email address should be of valid format' do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @author.email = valid_address
      assert @author.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test 'email validation should reject invalid addresses' do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @author.email = invalid_address
      assert_not @author.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test 'email addresses should be unique' do
    duplicate_author = @author.dup
    @author.save
    duplicate_author.email.downcase
    assert_not duplicate_author.valid?
  end

  test 'password should be present(non blank)' do
    @author.password = @author.password_confirmation = ' '*6
    assert_not @author.valid?
  end

  test 'password should not be too short' do
    @author.password = @author.password_confirmation = 'a'*5
    assert_not @author.valid?
  end
end
