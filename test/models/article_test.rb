require 'test_helper'

class ArticleTest < ActiveSupport::TestCase

  def setup
    @article = Article.new(name: 'First Article', content: 'Lorem Ipsum')
  end

  test 'should be valid' do
    assert @article.valid?
  end

  test 'name should be present' do
    @article.name = ''
    assert_not @article.valid?
  end

  test 'content should be present' do
    @article.content = ''
    assert_not @article.valid?
  end

  test 'name should not be too long' do
    @article.name = 'a'*101
    assert_not @article.valid?
  end
end
