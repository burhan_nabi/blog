require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @article = articles(:one)
    @comment = Comment.new( name:'Burhan Nabi', comment_text:'What up bitches?', article_id:@article.id)
  end

  test 'comment should be valid' do
    assert @comment.valid?
  end

  test 'name should be of valid length' do
    @comment.name = 'a'*3
    assert_not @comment.valid?

    @comment.name = 'a'*31
    assert_not @comment.valid?
  end

  test 'comment should be of valid length' do
    @comment.comment_text = 'a'*3
    assert_not @comment.valid?

    @comment.comment_text = 'a'*1201
    assert_not @comment.valid?
  end

  test 'name should be present' do
    @comment.name = ''
    assert_not @comment.valid?
  end

  test 'comment should be present' do
    @comment.comment_text = ''
    assert_not @comment.valid?
  end
end
