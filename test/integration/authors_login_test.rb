require 'test_helper'

class AuthorsLoginTest < ActionDispatch::IntegrationTest

  def setup
    @author = authors(:test)
  end

  test 'login with invalid information' do
    get login_path
    assert_template 'sessions/new'
    post login_path, session: {email:'', password:''}
    assert_template 'sessions/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end

  test 'login with valid credentials' do
    get login_path
    post login_path, session: {email: @author.email, password: 'password'}
    assert_redirected_to @author
    follow_redirect!
    assert_template 'authors/show'
    assert_select 'a[href=?]', login_path, count: 0
    assert_select 'a[href=?]', logout_path
    assert_select 'a[href=?]', author_path(@author)
  end
end
