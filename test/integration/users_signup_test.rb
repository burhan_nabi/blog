require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

  test 'invalid signup information' do
    get new_user_path
    assert_no_difference 'User.count' do
      post users_path, user: { name: "", email: "user@invalid",
                     password:'', password_confirmation:''}
    end
    assert_template "users/new"
  end

  test 'valid signup information' do
    get new_user_path
    assert_difference 'User.count', 1 do
      post users_path, user: {name:'Example User',
                     email:'example@gmail.com',
                     password:'foobar',
                     password_confirmation:'foobar',
                     id: nil}
    end
    assert_template 'users/show'
  end
end
