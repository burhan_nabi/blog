class AddAuthenticatedToAuthors < ActiveRecord::Migration
  def change
    add_column :authors, :authenticated, :boolean
  end
end
