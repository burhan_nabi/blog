class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :name
      t.text :comment_text
      t.integer :num_likes
      t.references :article, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
