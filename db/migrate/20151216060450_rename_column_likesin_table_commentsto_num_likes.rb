class RenameColumnLikesinTableCommentstoNumLikes < ActiveRecord::Migration
  def change
    rename_column :comments, :likes, :num_likes
  end
end
