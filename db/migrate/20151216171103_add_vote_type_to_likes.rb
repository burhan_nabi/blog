class AddVoteTypeToLikes < ActiveRecord::Migration
  def change
    add_column :likes, :vote_type, :integer
  end
end
