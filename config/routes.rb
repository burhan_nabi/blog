Rails.application.routes.draw do
  get 'feedback/new'

  resources :users

  root 'main#home'

  get 'home'    => 'main#home'
  get 'contact' => 'main#contact'
  get 'login'   => 'sessions#new'
  post 'login'  => 'sessions#create'
  get 'logout'  => 'sessions#destroy'

  post 'downvote' => 'sessions#downvote'
  post 'upvote' => 'sessions#upvote'
  post 'toggleFav' => 'sessions#toggleFav'

  post 'feedbacks' =>'feedback#create'

  get 'search-articles' => 'search#search_articles'

  # NEW THING LEARNT
  # /yikesydikes maps to author/new and linked via the new_author_path
  # instead of yikesdikes_path
  get 'yikesydikes', to: 'authors#new', as: :new_author
  post 'yikesydikes', to: 'authors#create', as: :post_author
  get '/authors/:id', to: 'authors#show', as: :author

  resources :articles do
    resources :comments, only:[:create, :destroy]
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
